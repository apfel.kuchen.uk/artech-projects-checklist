# artech-projects-checklist

1. Médecin Sans Frontières Fund Raising App - https://gitlab.com/apfel.kuchen.uk/msf
2. Pulpage Website with CMS - https://gitlab.com/apfel.kuchen.uk/simple-cms-1
3. Media Server - https://gitlab.com/apfel.kuchen.uk/media-server
4. Notes Management System - https://gitlab.com/apfel.kuchen.uk/notes-mastery-stable
5. Pamon Kiosk - https://gitlab.com/apfel.kuchen.uk/pamon-kiosk
6. Pamon Order Taking Application - https://gitlab.com/apfel.kuchen.uk/pamonordertaker
7. Pamon Web Server and Product Management System - https://gitlab.com/apfel.kuchen.uk/pamon
8. Pamon Management System Client Web Application - https://gitlab.com/apfel.kuchen.uk/react-cms
9. Web Server with CMS for Kinfu and Moneybarter - https://gitlab.com/apfel.kuchen.uk/wonderedu
10. CMS Client for Kinfu and Moneybarter - https://gitlab.com/apfel.kuchen.uk/react-admin
11. Rausch Web Server with CMS - https://gitlab.com/apfel.kuchen.uk/rausch
12. Rausch CMS Client - https://gitlab.com/apfel.kuchen.uk/rausch-cms

